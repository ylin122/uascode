#include "NavigatorSim.hpp"
#include "common/UserStructs/constants.h"
#include "common/Utils/GetTimeUTC.h"
#include "Planner/UserStructs/PlaneStateSim.h"
//other
#include "armadillo"

using namespace UasCode;

int main(int argc,char** argv)
{
  NavigatorSim navigator;
  //parameters
  double _Tmax= 12.49*CONSTANT_G;
  double _Muav= 29.2; //kg
  double myaw_rate= 10./180*M_PI;
  double mpitch_rate= 5./180*M_PI;
  double _max_speed= 35; //m/s
  double _min_speed= 20; //m/s
  double _max_pitch= 20./180*M_PI;
  double _min_pitch= -20./180*M_PI;
  
  double dt= 1;
  double _speed_trim= 30.;

  navigator.UpdaterSetParams(_Tmax,mpitch_rate,myaw_rate,_Muav,
      _max_speed,_min_speed,_max_pitch,_min_pitch);

  navigator.TECsReadParams("parameters.txt");
  navigator.L1SetRollLim(10./180*M_PI);
  navigator.SetDt(dt);
  navigator.SetSpeedTrim(_speed_trim);
  //test
  arma::vec::fixed<2> pt_A, pt_B;
  //pt_A<<33.385903<<-111.965008;
  //pt_B<<33.387336<<-111.837292;
//  double lat=33.433477,lon=-111.901494;
  double lat= 33.440081, lon= -112.029698;
  double lat1=33.437753,lon1= -111.985238;
  pt_A << lat << lon;
  pt_B << lat1 << lon1;

  double hgt= 1000;
  double spd= 25;
  double t= Utils::GetTimeUTC();
  double yaw= 90./180*M_PI;
  double pitch= 0.;
  double alt_B= 1100;
  UserStructs::PlaneStateSim st(t,0,0,lat,lon,hgt,spd,yaw,pitch,0,0,0); 
  //target mission point
  UserStructs::MissionSimPt Pt(pt_B(0),pt_B(1),alt_B,yaw,100,0,0,2000,2500,150);
  /*
  UserStructs::PlaneStateSim st;
  UserStructs::MissionSimPt Pt;

  std::fstream myfile("out_two.txt"); 
  if(myfile.is_open()){
    std::string line;
    std::getline(myfile,line);
    std::istringstream iss(line);
    //get the state
    iss >> st.t
        >> st.x
	>> st.y
	>> st.lat
	>> st.lon
	>> st.z
	>> st.speed
	>> st.yaw
	>> st.pitch
	>> st.ax >> st.ay >> st.az;
    //get wp
    std::getline(myfile,line);
    iss.str(line);
    iss >> Pt.lat
        >> Pt.lon
	>> Pt.alt
	>> Pt.yaw
	>> Pt.r
	>> Pt.x
	>> Pt.y
	>> Pt.h_rec
	>> Pt.v_rec
	>> Pt.alt_rec;
    
  }
  pt_A << st.lat << st.lon;
  */
  //future state
  UserStructs::PlaneStateSim st_end;
  //go one step
  //navigator.PropagateStep(st,st_end,pt_A,Pt);
  //go to the waypoint
  int result= navigator.PropagateWp(st,st_end,pt_A,Pt);
  std::cout<< "result: "<< result<< std::endl;
}
